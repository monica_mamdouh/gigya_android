package com.example.monicamamdouh.gigya.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.example.monicamamdouh.gigya.R;
import com.example.monicamamdouh.gigya.common.base.BaseActivity;
import com.example.monicamamdouh.gigya.common.helper.GigyaProvider;
import com.example.monicamamdouh.gigya.common.models.UserProfile;

public class ActivityMain extends BaseActivity {
    private UserProfile userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userProfile = getIntent().getParcelableExtra(GigyaProvider.GigyaConstants.USER_KEY);
        loadFragment();
    }

    @Override
    protected void initializeViews() {

    }

    @Override
    protected void setListeners() {

    }

    private void loadFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_main, FragmentMain.newInstance(userProfile)).commit();

    }

    public static void startActivityMain(Context context, UserProfile userProfile) {
        Intent i = new Intent(context, ActivityMain.class);
        i.putExtra(GigyaProvider.GigyaConstants.USER_KEY, userProfile);
        ((Activity) context).finish();
        context.startActivity(i);
    }

}
