package com.example.monicamamdouh.gigya.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.monicamamdouh.gigya.R;
import com.example.monicamamdouh.gigya.common.base.BaseFragment;
import com.example.monicamamdouh.gigya.common.models.UserProfile;
import com.example.monicamamdouh.gigya.main.ActivityMain;
import com.example.monicamamdouh.gigya.registration.ActivityRegistration;

public class FragmentLogin extends BaseFragment implements LoginView {
    private EditText passwordEditText;
    private EditText mailEditText;
    private Button loginButton;
    private Button twitterButton;
    private Button googleButton;
    private Button facebookButton;
    private Button createAccountButton;

    private LoginPresenter loginPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_login, container, false);
        initializeViews(v);
        setListeners();
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loginPresenter = new LoginPresenter(this);
    }

    @Override
    protected void initializeViews(View v) {

        passwordEditText = v.findViewById(R.id.edtPassword);
        mailEditText = v.findViewById(R.id.edtEmail);
        loginButton = v.findViewById(R.id.btnLogin);
        facebookButton = v.findViewById(R.id.btnFacebook);
        twitterButton = v.findViewById(R.id.btnTwitter);
        googleButton = v.findViewById(R.id.btnGoogle);
        createAccountButton = v.findViewById(R.id.btnCreateAccount);
    }

    @Override
    protected void setListeners() {

        loginButton.setOnClickListener(btnLoginClickListener);
        facebookButton.setOnClickListener(btnFacebookClickListener);
        googleButton.setOnClickListener(btnGoogleClickListener);
        twitterButton.setOnClickListener(btnTwitterClickListener);
        createAccountButton.setOnClickListener(btnCreateAccountListener);


    }


    private View.OnClickListener btnCreateAccountListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ActivityRegistration.startActivityRegistration(getActivity());
        }
    };

    private View.OnClickListener btnFacebookClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            loginPresenter.loginUsingFacebook(getActivity(), getContext());
        }
    };
    private View.OnClickListener btnGoogleClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            loginPresenter.loginUsingGoogle(getActivity(), getContext());
        }
    };
    private View.OnClickListener btnTwitterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            loginPresenter.loginUsingTwitter(getActivity(), getContext());
        }
    };
    private View.OnClickListener btnLoginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String mail = mailEditText.getText().toString().trim();
            String password = passwordEditText.getText().toString().trim();
            loginPresenter.login(mail, password, getContext());

        }
    };


    public static FragmentLogin newInstance() {
        return new FragmentLogin();
    }


    @Override
    public void OnUserLogin(UserProfile result) {
        //handle login data
        // Toast.makeText(getActivity(), result.getFirstName(), Toast.LENGTH_LONG).show();
        ActivityMain.startActivityMain(getActivity(), result);

    }

    @Override
    public void OnUserLoginError(String errorTxt) {
        Toast.makeText(getActivity(), errorTxt, Toast.LENGTH_LONG).show();

    }
}
