package com.example.monicamamdouh.gigya.common.helper;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.gigya.socialize.android.GSAPI;


/**
 * Created by Monica.Mamdouh on 11/14/2017.
 */

public class MyApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        GSAPI.getInstance().initialize(getApplicationContext(),Constants.GIGYA_API_KEY);
        FacebookSdk.sdkInitialize(getApplicationContext());


    }
}
