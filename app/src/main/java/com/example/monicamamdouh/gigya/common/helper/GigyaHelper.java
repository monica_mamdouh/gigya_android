package com.example.monicamamdouh.gigya.common.helper;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.example.monicamamdouh.gigya.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Monica.Mamdouh on 11/15/2017.
 */

public class GigyaHelper {


    public static Boolean validate(String firstName, String lastName, String email, String password, String confirmPassword, boolean socialPending, Context context) {
        if (TextUtils.isEmpty(firstName.trim())) {
            Toast.makeText(context,
                    context.getResources().getString(R.string.missingFirstName),
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (TextUtils.isEmpty(lastName.trim())) {
            Toast.makeText(context,
                    context.getResources().getString(R.string.missingLastName),
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (TextUtils.isEmpty(email.trim())) {
            Toast.makeText(context,
                    context.getResources().getString(R.string.missingEmail),
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (!socialPending && !isEmailValid(email)) {
            Toast.makeText(context,
                    context.getResources().getString(R.string.emailValidationStr),
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (!socialPending && TextUtils.isEmpty(password)) {
            Toast.makeText(context,
                    context.getResources().getString(R.string.missingPassword),
                    Toast.LENGTH_LONG).show();

            return false;
        } else if (!socialPending && password.trim().length() < 6) {
            Toast.makeText(context,
                    context.getResources().getString(R.string.passwordLessThan6),
                    Toast.LENGTH_LONG).show();
            return false;

        } else if (!socialPending && TextUtils.isEmpty(confirmPassword)) {
            Toast.makeText(context,
                    context.getResources().getString(R.string.missingConfirmPassword),
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (!socialPending && !confirmPassword.equals(password)) {
            Toast.makeText(
                    context,
                    context.getResources().getString(R.string.confirmPassValidationStr),
                    Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


}

