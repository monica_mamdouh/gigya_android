package com.example.monicamamdouh.gigya.common.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;

import com.example.monicamamdouh.gigya.common.models.UserProfile;
import com.example.monicamamdouh.gigya.login.ActivityLogin;
import com.facebook.login.LoginManager;
import com.gigya.socialize.GSObject;
import com.gigya.socialize.android.GSAPI;
import com.gigya.socialize.android.GSSession;

import static com.example.monicamamdouh.gigya.common.helper.GigyaProvider.GigyaConstants.DATA;
import static com.example.monicamamdouh.gigya.common.helper.GigyaProvider.GigyaConstants.EMAIL_KEY;
import static com.example.monicamamdouh.gigya.common.helper.GigyaProvider.GigyaConstants.FIRST_NAME_KEY;
import static com.example.monicamamdouh.gigya.common.helper.GigyaProvider.GigyaConstants.GIGYA_REGISTER;
import static com.example.monicamamdouh.gigya.common.helper.GigyaProvider.GigyaConstants.LAST_NAME_KEY;
import static com.example.monicamamdouh.gigya.common.helper.GigyaProvider.GigyaConstants.NEWS_LETTERS;
import static com.example.monicamamdouh.gigya.common.helper.GigyaProvider.GigyaConstants.PASSWORD_KEY;
import static com.example.monicamamdouh.gigya.common.helper.GigyaProvider.GigyaConstants.PROFILE_KEY;
import static com.example.monicamamdouh.gigya.common.helper.GigyaProvider.GigyaConstants.PROMOTION;
import static com.example.monicamamdouh.gigya.common.helper.GigyaProvider.GigyaConstants.REQ_KEY;
import static com.example.monicamamdouh.gigya.common.helper.GigyaProvider.GigyaConstants.REQ_ORIGIN;
import static com.example.monicamamdouh.gigya.common.helper.GigyaProvider.GigyaConstants.TERMS;
import static com.example.monicamamdouh.gigya.common.helper.GigyaProvider.GigyaConstants.lOGIN_ID;

/**
 * Created by Monica.Mamdouh on 11/14/2017.
 */

public class GigyaProvider {
    private String regToken = null;
    private String queuedMethod = null;
    private GSObject queuedParams = null;
    private String queuedObject = null;
    private RegisterationType currentRegisterType;


    public enum RegisterationType {
        APP,
        FACEBOOK,
        TWITTER,
        GOOGLEPLUS
    }

    private static final String[] GIGYA_PERMISSIONS = {
            Manifest.permission.GET_ACCOUNTS,
    };

    public class GigyaConstants {
        static final String REQ_STATUS_KEY = "statusReason";
        // static final String GIGYA_LARGE_PROFILE_PIC_KEY = "photoURL";
        static final String FIRST_NAME_KEY = "firstName";
        static final String LAST_NAME_KEY = "lastName";
        static final String EMAIL_KEY = "email";
        static final String REQ_KEY = "regToken";


        static final String PROVDER = "provider";
        public static final String USER_KEY = "UserProfile";
        static final String DATA = "data";
        static final String TERMS = "terms";
        static final String NEWS_LETTERS = "newsletters";
        static final String PROMOTION = "promotion";
        static final String REQ_ORIGIN = "registrationOrigin";
        static final String REQ_STATUS_OK = "OK";
        static final String PROFILE_KEY = "profile";
        static final String PASSWORD_KEY = "password";
        static final String lOGIN_ID = "loginID";
        static final String GIGYA_REGISTER = "accounts.register";
        static final String GET_ACCOUNT_INFO = "accounts.getAccountInfo";
        static final String SET_ACCOUNT_INFO = "accounts.setAccountInfo";

        //Twitter
        public final static String PROVIDER_TWITTER = "twitter";

        //facebook
        public final static String PROVIDER_FACEBOOK = "facebook";

        //google
        public final static String PROVIDER_GOOGLE_PLUS = "googleplus";
        //registration

        public final static String packageName = "com.example.monicamamdouh.gigya";

        public final static String GIGYA_INIT_REGISTRATION = "accounts.initRegistration";
        public final static String GIGYA_LOGIN_USING_MAIL = "accounts.login";
        public final static String GIGYA_LOGIN_USING_SOCIAL = "login";

        public final static String GIGYA_FINALIZE_REGISTER = "accounts.finalizeRegistration";


    }

    private static GigyaProvider gigyaProvider;

    private GigyaProvider() {


    }


    public static GigyaProvider getInstance() {
        if (gigyaProvider == null) {
            gigyaProvider = new GigyaProvider();
        }
        return gigyaProvider;
    }


    //This method is called to get token to pass to Register method
    void getTokenInitRegistration(GigyaListener gigyaListener, Context context) {
        sendRequest(GigyaConstants.GIGYA_INIT_REGISTRATION, null, null, gigyaListener, context);

    }

    public static void sendRequest(String method, GSObject params, String object, GigyaListener<String> gigyaListener, Context context) {
        try {
            GSAPI.getInstance().sendRequest(method, params, true, new GigyaResponseListener(gigyaListener, context), object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void loginUserUsingSocialMedia(Activity activity, String provider, Context context, final GigyaListener<UserProfile> gigyaListener) {

        try {

            GSObject params = new GSObject();
            params.put(GigyaConstants.PROVDER, provider);
            GSAPI.getInstance().login(activity, params, new GigyaResponseListener(gigyaListener, context), provider);
        } catch (Exception e) {
            gigyaListener.onError(e.toString());

        }
    }

    public void registerSocialMedia(String firstName, String lastName, String email, RegisterationType type, Context context, GigyaListener<String> gigyaListener) {
        try {
            // profile object
            currentRegisterType = type;
            GSObject profile = new GSObject();
            profile.put(FIRST_NAME_KEY, firstName);
            profile.put(LAST_NAME_KEY, lastName);
            profile.put(EMAIL_KEY, email);
            // profile data
            GSObject data = new GSObject();
            data.put(TERMS, true);
            data.put(NEWS_LETTERS, false);
            data.put(PROMOTION, false);
            // request params
            GSObject params = new GSObject();
            params.put(REQ_KEY, regToken);
            params.put(PROFILE_KEY, profile);
            params.put(DATA, data);


            sendRequest(GigyaConstants.SET_ACCOUNT_INFO, params, null, gigyaListener, context);


        } catch (Exception e) {
            gigyaListener.onError(e.toString());
        }
    }

    public void registerNewUser(String firstName, String lastName,
                                String email, String password, Context context, GigyaListener<String> gigyaListener) {

        try {
            // profile object
            GSObject profile = new GSObject();
            profile.put(FIRST_NAME_KEY, firstName);
            profile.put(LAST_NAME_KEY, lastName);


            // profile data
            GSObject data = new GSObject();
            data.put(TERMS, true);
            data.put(NEWS_LETTERS, false);
            data.put(PROMOTION, false);
            data.put(REQ_ORIGIN, Constants.packageName);

            // request params
            GSObject params = new GSObject();
            params.put(EMAIL_KEY, email);
            params.put(PASSWORD_KEY, password);
            params.put(PROFILE_KEY, profile);
            params.put(DATA, data);


            if (regToken == null) {  //if reqToken is null , get it by calling getTokenInitRegistration
                queueOperation(GigyaConstants.GIGYA_REGISTER, params, null);
                getTokenInitRegistration(gigyaListener, context);
            } else {
                params.put(REQ_KEY, regToken);
                sendRequest(GigyaConstants.GIGYA_REGISTER, params, null, gigyaListener, context);
            }
        } catch (Exception e) {
            gigyaListener.onError(e.toString());
        }

    }

    void queueOperation(String method, GSObject params, String object) {
        try {
            this.queuedMethod = method;
            this.queuedParams = params;
            this.queuedObject = object;
        } catch (Exception e) {
            e.printStackTrace();

        }

    }
    public void logOut(Context context) {
        GSSession session = GSAPI.getInstance().getSession();
        if (session != null && session.isValid()) {
            GSAPI.getInstance().logout();
            ActivityLogin.startActivityLogin(context);
        }

    }

    public void loginUserUsingEmail(String mail, String password, Context context, final GigyaListener<UserProfile> gigyaListener) {

        try {

            GSObject params = new GSObject();
            params.put(lOGIN_ID, mail);
            params.put(PASSWORD_KEY, password);

            GSAPI.getInstance().sendRequest(GigyaConstants.GIGYA_LOGIN_USING_MAIL, params, true, new GigyaResponseListener(gigyaListener, context), null);

        } catch (Exception e) {
            gigyaListener.onError(e.toString());

        }
    }

    public void executeQueuedOperation(GigyaListener gigyaListener, String regToken, Context context) {
        try {
            if (queuedMethod != null) {
                if (queuedMethod.equals(GIGYA_REGISTER)) {
                    queuedParams.put(REQ_KEY, regToken);
                }
                GigyaProvider.sendRequest(queuedMethod, queuedParams, queuedObject, gigyaListener, context);
                queuedMethod = null;
                queuedParams = null;
                queuedObject = null;
            }
        } catch (Exception e) {
            gigyaListener.onError(e.toString());

        }
    }


}
