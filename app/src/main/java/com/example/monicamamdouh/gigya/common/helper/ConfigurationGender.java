package com.example.monicamamdouh.gigya.common.helper;

/**
 * Created by Monica.Mamdouh on 11/21/2017.
 */

public class ConfigurationGender {
    public enum GENDER {
        MALE("m"), FEMALE("f"), UNDEFINED("");
        String value;

        GENDER(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }


}
