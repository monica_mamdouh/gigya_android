package com.example.monicamamdouh.gigya.login;

import android.app.Activity;
import android.content.Context;

import com.example.monicamamdouh.gigya.common.helper.GigyaListener;
import com.example.monicamamdouh.gigya.common.helper.GigyaProvider;
import com.example.monicamamdouh.gigya.common.models.UserProfile;

/**
 * Created by Monica.Mamdouh on 11/14/2017.
 */

class LoginPresenter {
    private LoginView loginView;

    LoginPresenter(LoginView loginView) {

        this.loginView = loginView;
    }

    void loginUsingFacebook(Activity activity, Context context) {
        GigyaProvider.getInstance().loginUserUsingSocialMedia(activity, GigyaProvider.GigyaConstants.PROVIDER_FACEBOOK, context, new GigyaListener<UserProfile>() {
            @Override
            public void onSuccess(UserProfile userProfile) {
                loginView.OnUserLogin(userProfile);

            }

            @Override
            public void onError(String errorCode) {
                loginView.OnUserLoginError(errorCode);

            }


        });
    }

    void loginUsingGoogle(Activity activity, Context context) {
        GigyaProvider.getInstance().loginUserUsingSocialMedia(activity, GigyaProvider.GigyaConstants.PROVIDER_GOOGLE_PLUS, context, new GigyaListener<UserProfile>() {
            @Override
            public void onSuccess(UserProfile userProfile) {
                loginView.OnUserLogin(userProfile);

            }

            @Override
            public void onError(String errorCode) {
                loginView.OnUserLoginError(errorCode);

            }


        });
    }

    void loginUsingTwitter(Activity activity, Context context) {
        GigyaProvider.getInstance().loginUserUsingSocialMedia(activity, GigyaProvider.GigyaConstants.PROVIDER_TWITTER, context, new GigyaListener<UserProfile>() {
            @Override
            public void onSuccess(UserProfile userProfile) {
                loginView.OnUserLogin(userProfile);

            }

            @Override
            public void onError(String errorCode) {
                loginView.OnUserLoginError(String.valueOf(errorCode));

            }

        });
    }

    void login(String mail, String password, Context context) {
        GigyaProvider.getInstance().loginUserUsingEmail(mail, password, context, new GigyaListener<UserProfile>() {
            @Override
            public void onSuccess(UserProfile userProfile) {
                loginView.OnUserLogin(userProfile);

            }

            @Override
            public void onError(String errorCode) {
                loginView.OnUserLoginError(String.valueOf(errorCode));

            }

        });
    }
}
