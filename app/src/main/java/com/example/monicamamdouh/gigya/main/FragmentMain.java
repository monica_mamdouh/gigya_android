package com.example.monicamamdouh.gigya.main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.monicamamdouh.gigya.R;
import com.example.monicamamdouh.gigya.common.base.BaseFragment;
import com.example.monicamamdouh.gigya.common.helper.GigyaProvider;
import com.example.monicamamdouh.gigya.common.models.UserProfile;
import com.example.monicamamdouh.gigya.registration.FragmentRegistration;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMain extends BaseFragment {

    private UserProfile userProfile;
    private Button logOutButton;
    private TextView nameUserTextView;

    public FragmentMain() {
        // Required empty public constructor
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            userProfile = getArguments().getParcelable(GigyaProvider.GigyaConstants.USER_KEY);
        }
        if(userProfile!=null) {
            nameUserTextView.setText(userProfile.getFirstName());
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        initializeViews(v);
        setListeners();
        return v;
    }

    @Override
    protected void initializeViews(View v) {

        logOutButton=v.findViewById(R.id.logout_button);
       // photoUserImageView.setImageDrawable(userProfile.getPhotoUrl());
        nameUserTextView=v.findViewById(R.id.userName);

    }

    @Override
    protected void setListeners() {

        logOutButton.setOnClickListener(logOutClickListener);
    }
    private View.OnClickListener logOutClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            GigyaProvider.getInstance().logOut(getActivity());
        }
    };
    public static FragmentMain newInstance(UserProfile userProfile) {
        FragmentMain fragmentMain=new FragmentMain();
        Bundle bundle = new Bundle();
        bundle.putParcelable(GigyaProvider.GigyaConstants.USER_KEY, userProfile);
        fragmentMain.setArguments(bundle);
        return fragmentMain;
    }

}
