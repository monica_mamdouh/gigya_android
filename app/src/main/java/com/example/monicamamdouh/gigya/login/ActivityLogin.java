package com.example.monicamamdouh.gigya.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.example.monicamamdouh.gigya.R;
import com.example.monicamamdouh.gigya.common.base.BaseActivity;
import com.example.monicamamdouh.gigya.registration.ActivityRegistration;

public class ActivityLogin extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loadFragment();
    }

    private void loadFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_login, FragmentLogin.newInstance()).commit();

    }

    @Override
    protected void initializeViews() {

    }


    @Override
    protected void setListeners() {

    }
    public static void startActivityLogin(Context context) {
        Intent i = new Intent(context, ActivityLogin.class);
        context.startActivity(i);
        ((Activity) context).finish();

    }


}