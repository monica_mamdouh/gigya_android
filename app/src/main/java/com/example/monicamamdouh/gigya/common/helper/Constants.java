package com.example.monicamamdouh.gigya.common.helper;

/**
 * Created by Monica.Mamdouh on 11/14/2017.
 */

public class Constants {
    public static final String PROFILE="profile";
    public static final String FIRST_NAME="firstName";
    public static final String LAST_NAME="lastName";
    public static final String EMAIL="email";
    public static final String COUNTRY="country";
    public static final String BIRTHDAY="birthDay";
    public static final String BIRTH_MONTH="birthMonth";
    public static final String BIRTH_YEAR="birthYear";
    public static final String AGE="age";
    public static final String PHOTO_URL="photoURL";
    public static final String GENDER="gender";
    public static final String LOGIN_PROVIDER="loginProvider";
    public static final String SOCIAL_PROVIDER="socialProviders";
    public static final String FEMALE="f";
    public static final String PROVIDERS="providers";
    public static final String UID="UID";
    public static final String FACEBOOK="facebook";
    public static final String TWITTER="twitter";
    public static final String GOOGLE="google";
    public static final String SITE="site";

    public static final String GIGYA_API_KEY = "3_pThPrs0T6QvcmZj0jU3pYXsQv8lx-WkhAsIOidvN9YMB3SP7nFI6T9iebGKLThRq";

    //public static final String GIGYA_API_KEY  = "3_WD_OxYwhtPOQX1sUOjYdQW26zLVRRaIJ0SMNC_BgSEFgOhAgChlSaUj39FW3JS4M";
    public static final String packageName = "com.example.monicamamdouh.gigya";
    public static final String FORGOTPASSWORDURL = "http://www.mbc.net/ar/reset-password.html";


}
