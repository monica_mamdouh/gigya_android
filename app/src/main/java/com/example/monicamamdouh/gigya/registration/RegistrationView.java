package com.example.monicamamdouh.gigya.registration;

import com.example.monicamamdouh.gigya.common.base.BaseView;

/**
 * Created by Monica.Mamdouh on 11/20/2017.
 */

interface RegistrationView extends BaseView {
    void onUserRegistered(String result);

    void onUserRegisteredError(String error);


}
