package com.example.monicamamdouh.gigya.registration;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.monicamamdouh.gigya.R;
import com.example.monicamamdouh.gigya.common.base.BaseFragment;
import com.example.monicamamdouh.gigya.common.helper.GigyaHelper;
import com.example.monicamamdouh.gigya.common.helper.GigyaProvider;
import com.example.monicamamdouh.gigya.common.models.UserProfile;

public class FragmentRegistration extends BaseFragment implements RegistrationView {

    private EditText firstNamedEditText;
    private EditText lastNameEditText;
    private EditText passwordEditText;
    private EditText confirmedPasseordEditText;
    private EditText emailEditText;
    private Button registerButton;
    private String firstName, lastName, email, password, confirmPassword;
    private RegistrationPresenter registrationPresenter;
    private UserProfile userProfile;
    private boolean socialPending = false;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registrationPresenter = new RegistrationPresenter(this);
        if (getArguments() != null) {
            userProfile = getArguments().getParcelable(GigyaProvider.GigyaConstants.USER_KEY);
        }

        if (userProfile != null) {
            socialPending = true;
            loadSocialData();
        }

    }

    void loadSocialData() {

        if (userProfile.getFirstName() != null)
            firstNamedEditText.setText(userProfile.getFirstName());
        if (userProfile.getLastName() != null)
            lastNameEditText.setText(userProfile.getLastName());
        if (userProfile.getEmail() != null)
            emailEditText.setText(userProfile.getEmail());
        passwordEditText.setVisibility(View.GONE);
        confirmedPasseordEditText.setVisibility(View.GONE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_registration, container, false);
        initializeViews(v);
        setListeners();
        return v;
    }

    @Override
    protected void initializeViews(View v) {

        firstNamedEditText = v.findViewById(R.id.edtFirstName);
        lastNameEditText = v.findViewById(R.id.edtLastName);
        passwordEditText = v.findViewById(R.id.edtPassword);
        confirmedPasseordEditText = v.findViewById(R.id.edtConfirmPassword);
        emailEditText = v.findViewById(R.id.edtEmail);
        registerButton = v.findViewById(R.id.btnRegistration);


    }

    @Override
    protected void setListeners() {

        registerButton.setOnClickListener(btnRegistrationClickListener);
    }

    private View.OnClickListener btnRegistrationClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            firstName = firstNamedEditText.getText().toString();
            lastName = lastNameEditText.getText().toString();
            email = emailEditText.getText().toString();
            password = passwordEditText.getText().toString();
            confirmPassword = confirmedPasseordEditText.getText().toString();
            if (GigyaHelper.validate(firstName, lastName, email, password, confirmPassword, socialPending, getActivity())) {
                if (!socialPending) {
                    registrationPresenter.registrationUsingEmail(firstName, lastName, email, password, getContext());
                } else {
                    userProfile.setFirstName(firstName);
                    userProfile.setLastName(lastName);
                    userProfile.setEmail(email);
                    GigyaProvider.RegisterationType type = null;
                    if (userProfile.isFacebook())
                        type = GigyaProvider.RegisterationType.FACEBOOK;
                    else if (userProfile.isTwitter())
                        type = GigyaProvider.RegisterationType.TWITTER;
                    if (userProfile.isGoogle())
                        type = GigyaProvider.RegisterationType.GOOGLEPLUS;
                    registrationPresenter.registerUsingSocialMedia(firstName, lastName, email, type, getContext());
                }
            }

        }
    };

    public static FragmentRegistration newInstance(UserProfile userProfile) {
        FragmentRegistration fragmentRegistration = new FragmentRegistration();
        Bundle bundle = new Bundle();
        bundle.putParcelable(GigyaProvider.GigyaConstants.USER_KEY, userProfile);
        fragmentRegistration.setArguments(bundle);
        return fragmentRegistration;
    }


    @Override
    public void onUserRegistered(String result) {
        Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onUserRegisteredError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();

    }

}
