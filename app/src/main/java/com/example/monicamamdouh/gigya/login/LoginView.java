package com.example.monicamamdouh.gigya.login;

import com.example.monicamamdouh.gigya.common.base.BaseView;
import com.example.monicamamdouh.gigya.common.models.UserProfile;

/**
 * Created by Monica.Mamdouh on 11/14/2017.
 */

interface LoginView extends BaseView {
    void OnUserLogin(UserProfile userProfile);

    void OnUserLoginError(String errorCode);
}
