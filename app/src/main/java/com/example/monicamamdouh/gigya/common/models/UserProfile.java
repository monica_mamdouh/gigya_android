package com.example.monicamamdouh.gigya.common.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.monicamamdouh.gigya.common.helper.ConfigurationGender;
import com.example.monicamamdouh.gigya.common.helper.Constants;
import com.gigya.socialize.GSObject;

/**
 * Created by Monica.Mamdouh on 11/14/2017.
 */

public class UserProfile implements Parcelable {
    String name = "";
    String id = "";
    String photoUrl = "";
    ConfigurationGender.GENDER gender = ConfigurationGender.GENDER.MALE;
    String email = "";
    String firstName = "";
    String lastName = "";
    String country = "";
    int birthDay = 0;
    int birthMonth = 0;
    int birthYear = 0;
    int age = -1;
    boolean facebook = false;
    boolean twitter = false;
    boolean site = false;
    boolean google = false;

    public UserProfile(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<UserProfile> CREATOR = new Parcelable.Creator<UserProfile>() {
        public UserProfile createFromParcel(Parcel in) {
            return new UserProfile(in);
        }

        public UserProfile[] newArray(int size) {
            return new UserProfile[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int arg1) {
        dest.writeString(name);
        dest.writeString(id);
        dest.writeString(photoUrl);
        dest.writeValue(gender);
        dest.writeString(email);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(country);
        dest.writeInt(birthDay);
        dest.writeInt(birthMonth);
        dest.writeInt(birthYear);
        dest.writeInt(age);
        dest.writeValue(facebook);
        dest.writeValue(twitter);
        dest.writeValue(site);
        dest.writeValue(google);
    }

    public void readFromParcel(Parcel in) {
        name = in.readString();
        id = in.readString();
        photoUrl = in.readString();
        gender = (ConfigurationGender.GENDER) in.readValue(null);
        email = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        country = in.readString();
        birthDay = in.readInt();
        birthMonth = in.readInt();
        birthYear = in.readInt();
        age = in.readInt();
        facebook = (Boolean) in.readValue(null);
        twitter = (Boolean) in.readValue(null);
        site = (Boolean) in.readValue(null);
        google = (Boolean) in.readValue(null);

    }

    public UserProfile(GSObject user, boolean fromLogin) {
        if (fromLogin) {
            parseObjectFromLogin(user);
        } else {

            parseObjectFromSocial(user);
        }
    }

    // public AGTUserProfile(GraphUser user) {
    //
    // try {
    // if (user.getFirstName() != null) {
    // firstName = user.getFirstName();
    // }
    // if (user.getLastName() != null) {
    // lastName = user.getLastName();
    // }
    // if (user.getBirthday() != null) {
    // // String birthdayStr = user.getBirthday();
    // // birthDay=birthdayStr.su
    //
    // }
    // if (user.getProperty("email") != null) {
    // email = user.getProperty("email").toString();
    // }
    // if (user.getProperty("gender") != null) {
    // String genderStr = user.getProperty("gender").toString();
    // if (genderStr.equals("female"))
    // gender = GENDER.FEMALE;
    // else
    // gender = GENDER.MALE;
    // }
    //
    // } catch (Exception ex) {
    //
    // }
    // }

    void parseObjectFromLogin(GSObject user) {

        // {"UIDSignature":"Yk5HaU21Ldr2tkn/Cay4hVhrPxs=","lastUpdatedTimestamp":1412860720233,"socialProviders":"site","statusCode":200,"lastLoginTimestamp":1412963482000,"created":"2014-10-09T13:18:40.202Z","verified":"2014-10-09T13:19:18.283Z","oldestDataUpdated":"2014-10-09T13:18:40.233Z","sessionInfo":{"sessionSecret":"bjS22NZwK53+IcaVqv4TNA==","sessionToken":"AT3_A749903C569DDCE4958C8FE0A47C2B75_AaM2caZZxl1lgSTF9AR329rOzIkukGPhbiuFhWiYaRsi-7g1v9iC3LP57FApJsDYQ3je2tO0CDxmU2ETaggE4t2n0SIjNnI4G94rypOUBNvPZS09oAArozNBO7--wgP0FjvZfirI20G8tPDjdLXMyw=="},"isLockedOut":false,"profile":{"samlData":{},"lastName":"sss","birthYear":2004,"email":"agttest1@outlook.com","birthMonth":10,"age":10,"gender":"f","birthDay":9,"firstName":"fff","country":"������"},"isVerified":true,"createdTimestamp":1412860720202,"lastUpdated":"2014-10-09T13:18:40.233Z","isRegistered":true,"callId":"4f0903b030744cba8f5b187a263e4839","registerdTimestamp":1412860733238,"isActive":true,"oldestDataUpdatedTimestamp":1412860720233,"lastLogin":"2014-10-10T17:51:22Z","registered":"2014-10-09T13:18:53.238Z","UID":"a5b8824e2f1646478bfa01010f3c0020","statusReason":"OK","signatureTimestamp":"1412963482","verifiedTimestamp":1412860758283,"errorCode":0,"loginProvider":"site"}
        try {
            firstName = user.getObject(Constants.PROFILE).getString(Constants.FIRST_NAME);
        } catch (Exception e) {
        }
        try {
            lastName = user.getObject(Constants.PROFILE).getString(Constants.LAST_NAME);
        } catch (Exception e) {
        }
        try {
            email = user.getObject(Constants.PROFILE).getString(Constants.EMAIL);
        } catch (Exception e) {
        }
        try {
            country = user.getObject(Constants.PROFILE).getString(Constants.COUNTRY);
        } catch (Exception e) {
        }
        try {
            birthDay = user.getObject(Constants.PROFILE).getInt(Constants.BIRTHDAY);
        } catch (Exception e) {
        }
        try {
            birthMonth = user.getObject(Constants.PROFILE).getInt(Constants.BIRTH_MONTH);
        } catch (Exception e) {
        }
        try {
            birthYear = user.getObject(Constants.PROFILE).getInt(Constants.BIRTH_YEAR);
        } catch (Exception e) {
        }
        try {
            age = user.getObject(Constants.PROFILE).getInt(Constants.AGE);
        } catch (Exception e) {
        }
        try {
            photoUrl = user.getObject(Constants.PROFILE).getString(Constants.PHOTO_URL);
        } catch (Exception e) {
        }
        try {
            String temp = user.getObject(Constants.PROFILE).getString(Constants.GENDER);
            if (temp.equals(Constants.FEMALE)) {
                gender = ConfigurationGender.GENDER.FEMALE;
            }
        } catch (Exception e) {
            gender = ConfigurationGender.GENDER.UNDEFINED;
        }
        try {
            id = user.getString(Constants.UID);
        } catch (Exception e) {
        }
        try {
            String socialProviders = user.getString(Constants.SOCIAL_PROVIDER);
            if (socialProviders.contains(Constants.FACEBOOK)) {
                facebook = true;
            }
            if (socialProviders.contains(Constants.TWITTER)) {
                twitter = true;
            }
            if (socialProviders.contains(Constants.GOOGLE)) {
                google = true;
            }
            if (socialProviders.contains(Constants.SITE)) {
                site = true;
            }
        } catch (Exception e) {
        }

        try {
            String socialProviders = user.getString(Constants.PROVIDERS);
            if (socialProviders.contains(Constants.FACEBOOK)) {
                facebook = true;
            } else if (socialProviders.contains(Constants.TWITTER)) {
                twitter = true;
            } else if (socialProviders.contains(Constants.GOOGLE)) {
                google = true;
            } else if (socialProviders.contains(Constants.SITE)) {
                site = true;
            }
        } catch (Exception e) {
        }
    }

    void parseObjectFromSocial(GSObject user) {
        // {"oldestDataAge":1066632,"isSiteUID":false,"isTempUser":false,"birthMonth":"3","UIDSignature":"EXbmb+2dkQbYMQJIE70RHcyCOYs=","isLoggedIn":true,"timestamp":"2014-10-10 16:43:45","statusCode":200,"providers":"facebook,twitter,site","age":"30","gender":"m","firstName":"Nayera","thumbnailURL":"https://graph.facebook.com/100003680673570/picture?type=square","lastName":"Mohamed","birthYear":"1984","photoURL":"https://graph.facebook.com/100003680673570/picture?type=large","nickname":"Nayera Mohamed","isSiteUser":true,"identities":[{"lastName":"Mohamed","birthYear":"1984","photoURL":"https://graph.facebook.com/100003680673570/picture?type=large","nickname":"Nayera Mohamed","isLoginIdentity":true,"birthMonth":"3","lastUpdatedTimestamp":1412959423663,"mappedProviderUIDs":[{"apiKey":"3_CIgdRLm-RO1wSynYICxJECGE7LDVTkJlts3_0vwHqzjRpPauX1jHvhvq7bJANajd","providerUID":"100003680673570"}],"lastUpdated":"2014-10-10T16:43:43.663Z","provider":"facebook","isExpiredSession":false,"birthDay":"3","allowsLogin":true,"providerUID":"100003680673570","proxiedEmail":"","oldestDataUpdatedTimestamp":1412959423647,"email":"otlobtest@gmail.com","age":"30","oldestDataUpdated":"2014-10-10T16:43:43.647Z","gender":"f","profileURL":"https://www.facebook.com/nayera.mohamed.9","firstName":"Nayera","lastLoginTime":1412959423,"thumbnailURL":"https://graph.facebook.com/100003680673570/picture?type=square"},{"photoURL":"http://abs.twimg.com/sticky/default_profile_images/default_profile_6.png","nickname":"otlobtest","isLoginIdentity":false,"lastUpdatedTimestamp":1412959216291,"lastUpdated":"2014-10-10T16:40:16.291Z","provider":"twitter","isExpiredSession":false,"allowsLogin":true,"providerUID":"532980769","country":"","proxiedEmail":"","oldestDataUpdatedTimestamp":1412866250543,"oldestDataUpdated":"2014-10-09T14:50:50.543Z","profileURL":"http://twitter.com/otlobtest","firstName":"nayera","lastLoginTime":1412866250,"thumbnailURL":"http://abs.twimg.com/sticky/default_profile_images/default_profile_6_normal.png"},{"lastName":"Mohamed","birthYear":"1984","nickname":"Nayera Mohamed","isLoginIdentity":false,"birthMonth":"3","lastUpdatedTimestamp":1411892792643,"lastUpdated":"2014-09-28T08:26:32.643Z","provider":"site","isExpiredSession":false,"birthDay":"3","allowsLogin":false,"providerUID":"_guid_aaDQ8iJkgurgFTLXn2EjY9mpZuE33c9_o5mRax4slxQ=","country":"������� ������� ��������","proxiedEmail":"","oldestDataUpdatedTimestamp":1411892792643,"age":"30","oldestDataUpdated":"2014-09-28T08:26:32.643Z","gender":"m","firstName":"Nayera"}],"birthDay":"3","callId":"75ef68a2a6234aeebb543d554befcec0","country":"������� ������� ��������","proxiedEmail":"","oldestDataUpdatedTimestamp":1411892792643,"email":"otlobtest@gmail.com","UID":"_guid_aaDQ8iJkgurgFTLXn2EjY9mpZuE33c9_o5mRax4slxQ=","statusReason":"OK","signatureTimestamp":"1412959425","isConnected":true,"errorCode":0,"loginProviderUID":"100003680673570","UIDSig":"sNm+drIHS69o5rXS6b/vGpmrHKw=","capabilities":"Login, Friends, Notifications, Actions, Status, Photos, Contacts, Places, Checkins, FacebookActions","profileURL":"https://www.facebook.com/nayera.mohamed.9","loginProvider":"facebook"}
        try {
            firstName = user.getString(Constants.FIRST_NAME);
        } catch (Exception e) {
        }
        try {
            lastName = user.getString(Constants.LAST_NAME);
        } catch (Exception e) {
        }
        try {
            email = user.getString(Constants.EMAIL);
        } catch (Exception e) {
        }
        try {
            country = user.getString(Constants.COUNTRY);
        } catch (Exception e) {
        }
        try {
            birthDay = user.getInt(Constants.BIRTHDAY);
        } catch (Exception e) {
        }
        try {
            birthMonth = user.getInt(Constants.BIRTH_MONTH);
        } catch (Exception e) {
        }
        try {
            birthYear = user.getInt(Constants.BIRTH_YEAR);
        } catch (Exception e) {
        }
        try {
            String temp = user.getString(Constants.GENDER);
            if (temp.equals(Constants.FEMALE)) {
                gender = ConfigurationGender.GENDER.FEMALE;
            }
        } catch (Exception e) {
        }

        try {
            photoUrl = user.getString(Constants.PHOTO_URL);
        } catch (Exception e) {
        }
        try {
            id = user.getString(Constants.UID);

        } catch (Exception e) {
        }

        try {
            String socialProviders = user.getString(Constants.LOGIN_PROVIDER);
            if (socialProviders.contains(Constants.FACEBOOK)) {
                facebook = true;
            } else if (socialProviders.contains(Constants.TWITTER)) {
                twitter = true;
            } else if (socialProviders.contains(Constants.GOOGLE)) {
                google = true;
            } else if (socialProviders.contains(Constants.SITE)) {
                site = true;
            }
        } catch (Exception e) {
        }
    }

    public int getBirthDay() {
        return birthDay;
    }

    public int getBirthMonth() {
        return birthMonth;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public boolean isFacebook() {
        return facebook;
    }

    public boolean isTwitter() {
        return twitter;
    }

    public boolean isSite() {
        return site;
    }

    public boolean isGoogle() {
        return google;
    }

    public ConfigurationGender.GENDER getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCountry() {
        return country;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

