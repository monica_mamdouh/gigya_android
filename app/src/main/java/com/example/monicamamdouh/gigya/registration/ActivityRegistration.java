package com.example.monicamamdouh.gigya.registration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.example.monicamamdouh.gigya.R;
import com.example.monicamamdouh.gigya.common.base.BaseActivity;
import com.example.monicamamdouh.gigya.common.helper.GigyaProvider;
import com.example.monicamamdouh.gigya.common.models.UserProfile;

public class ActivityRegistration extends BaseActivity {
    private UserProfile userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        userProfile = getIntent().getParcelableExtra(GigyaProvider.GigyaConstants.USER_KEY);
        loadFragment();
    }

    private void loadFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_registration, FragmentRegistration.newInstance(userProfile)).commit();

    }

    @Override
    protected void initializeViews() {

    }

    @Override
    protected void setListeners() {

    }

    public static void startActivityRegistration(Context context) {
        Intent i = new Intent(context, ActivityRegistration.class);
        context.startActivity(i);
        ((Activity) context).finish();

    }

    public static void startActivityRegistration(Context context, UserProfile userProfile) {
        Intent i = new Intent(context, ActivityRegistration.class);
        i.putExtra(GigyaProvider.GigyaConstants.USER_KEY, userProfile);
        ((Activity) context).finish();
        context.startActivity(i);


    }


}
