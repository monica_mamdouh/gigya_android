package com.example.monicamamdouh.gigya.common.base;

import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

//    private Toolbar myToolbar;
//
//    protected void setToolbar(Toolbar toolbar, String title, boolean showUpButton, boolean withElevation) {
//        myToolbar = toolbar;
//        myToolbar.setTitle(title);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && withElevation) {
//            toolbar.setElevation(getResources().getDimension(R.dimen.padding_small));
//        }
//        setSupportActionBar(myToolbar);
//
//
//        if (showUpButton) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setDisplayHomeAsUpEnabled(true);
//            }
//        }
//    }
//
//    public void setToolbarTitle(String title) {
//        if (myToolbar != null)
//            myToolbar.setTitle(title);
//    }
//
//    public void setToolbarSubTitle(String subTitle) {
//        if (myToolbar != null) {
//            myToolbar.setSubtitle(subTitle);
//        }
//
//    }

    protected abstract void initializeViews();

    protected abstract void setListeners();

}
