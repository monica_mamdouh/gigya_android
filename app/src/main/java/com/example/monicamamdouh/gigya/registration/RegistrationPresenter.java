package com.example.monicamamdouh.gigya.registration;

import android.content.Context;

import com.example.monicamamdouh.gigya.common.base.BasePresenter;
import com.example.monicamamdouh.gigya.common.helper.GigyaListener;
import com.example.monicamamdouh.gigya.common.helper.GigyaProvider;

/**
 * Created by Monica.Mamdouh on 11/20/2017.
 */

public class RegistrationPresenter extends BasePresenter {
    private RegistrationView registrationView;

    RegistrationPresenter(RegistrationView registrationView) {

        this.registrationView = registrationView;
    }

    void registerUsingSocialMedia(String firstName, String lastName, String email, GigyaProvider.RegisterationType type, Context context) {
        GigyaProvider.getInstance().registerSocialMedia(firstName, lastName, email, type, context, new GigyaListener<String>() {
            @Override
            public void onSuccess(String result) {
                registrationView.onUserRegistered(result);

            }

            @Override
            public void onError(String errorCode) {
                registrationView.onUserRegisteredError(errorCode);


            }
        });
    }

    void registrationUsingEmail(String firstN, String lastN, String email, String password, Context context) {
        GigyaProvider.getInstance().registerNewUser(firstN, lastN, email, password, context, new GigyaListener<String>() {
            @Override
            public void onSuccess(String result) {
                registrationView.onUserRegistered(result);

            }

            @Override
            public void onError(String errorCode) {
                registrationView.onUserRegisteredError(errorCode);

            }
        });

    }
}
