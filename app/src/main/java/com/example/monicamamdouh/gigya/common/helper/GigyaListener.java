package com.example.monicamamdouh.gigya.common.helper;

/**
 * Created by Monica.Mamdouh on 11/14/2017.
 */

public interface GigyaListener<T> {
    void onSuccess(T result);

    void onError(String errorCode);

}
