package com.example.monicamamdouh.gigya.common.helper;

import android.content.Context;

import com.example.monicamamdouh.gigya.R;
import com.example.monicamamdouh.gigya.common.models.UserProfile;
import com.example.monicamamdouh.gigya.registration.ActivityRegistration;
import com.gigya.socialize.GSObject;
import com.gigya.socialize.GSResponse;
import com.gigya.socialize.GSResponseListener;
import com.gigya.socialize.android.GSAPI;

/**
 * Created by Monica.Mamdouh on 11/21/2017.
 */

public class GigyaResponseListener implements GSResponseListener {
    private GigyaListener gigyaListener;
    String regToken = null;
    private Context context;


    GigyaResponseListener(GigyaListener gigyaListener, Context context) {

        this.gigyaListener = gigyaListener;
        this.context = context;
    }

    @Override
    public void onGSResponse(String method, GSResponse response, Object object) {
        int errorCode = response.getErrorCode();

        try {

            if (method.equals(GigyaProvider.GigyaConstants.GIGYA_LOGIN_USING_MAIL) || method.equals(GigyaProvider.GigyaConstants.GIGYA_LOGIN_USING_SOCIAL)) {
                handleLoginResult(response, object, gigyaListener);
            } else if (method.equals(GigyaProvider.GigyaConstants.GET_ACCOUNT_INFO)) {
                handleGetAccountInfo(response);
            } else if (method.equals(GigyaProvider.GigyaConstants.SET_ACCOUNT_INFO)) {

                handleSetAccountInfo(errorCode, response, gigyaListener);

            }
            //Registration
            else if (method.equals(GigyaProvider.GigyaConstants.GIGYA_INIT_REGISTRATION)) {

                handleInitRegistrationResult(errorCode, response, gigyaListener, context);
            } else if (method.equals(GigyaProvider.GigyaConstants.GIGYA_REGISTER)) {
                handleRegistrationResult(errorCode, response, gigyaListener);
            } else if (method.equals(GigyaProvider.GigyaConstants.GIGYA_FINALIZE_REGISTER)) {
                handleFinalRegistrationResult(errorCode, gigyaListener);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void handleSetAccountInfo(int errorCode, GSResponse response, GigyaListener gigyaListener) {
        if (errorCode == 0) {
            try {
                finalizeRegistration(regToken, gigyaListener, context);

            } catch (Exception e) {
                gigyaListener.onError(e.toString());
            }
        }
    }

    void handleGetAccountInfo(GSResponse response) {
        try {

            int errorCode = response.getErrorCode();

            if (errorCode == 0) {
                GSObject profile = response.getData();
                UserProfile userProfile = new UserProfile(profile, true);
                ContinueRegistration(userProfile);
            }
        } catch (Exception e) {
            gigyaListener.onError(e.toString());
        }
    }

    void ContinueRegistration(UserProfile user) {

        ActivityRegistration.startActivityRegistration(context, user);

    }

    void handleLoginResult(GSResponse response, Object object, GigyaListener gigyaListener) {
        int errorCode = response.getErrorCode();

        try {
            if (errorCode == 0) {

                UserProfile userProfile = null;
                GSObject dataObject = response.getData();

                if (object == GigyaProvider.GigyaConstants.PROVIDER_FACEBOOK || object == GigyaProvider.GigyaConstants.PROVIDER_TWITTER || object == GigyaProvider.GigyaConstants.PROVIDER_GOOGLE_PLUS) {
                    userProfile = new UserProfile(dataObject, false);
                } else {
                    userProfile = new UserProfile(dataObject, true);
                }

                gigyaListener.onSuccess(userProfile);
            } else if (errorCode == 403042)// invalid Login ID
            {
                gigyaListener.onError(context.getResources().getString(R.string.invalidEmailPassword));
                //  LinkAccounts(response);


            } else if (errorCode == 206002)// account pending verification
            {
                gigyaListener.onError(context.getResources().getString(R.string.accountPendingVerification));


            } else if (errorCode == 206001) { // account registration pending
                regToken = response.getString(GigyaProvider.GigyaConstants.REQ_KEY, null);
                getAccountInfo(gigyaListener, context);
            }
        } catch (Exception ex) {
            gigyaListener.onError(ex.toString());

        }
    }

    public void getAccountInfo(GigyaListener gigyaListener, Context context) {
        try {
            GSObject params = new GSObject();
            params.put(GigyaProvider.GigyaConstants.REQ_KEY, regToken);
            GSAPI.getInstance().sendRequest(GigyaProvider.GigyaConstants.GET_ACCOUNT_INFO, params, true,
                    new GigyaResponseListener(gigyaListener, context), null);
        } catch (Exception e) {
            gigyaListener.onError(e.getMessage());
        }
    }

    public void finalizeRegistration(String regToken, GigyaListener gigyaListener, Context context) {
        try {
            GSObject params = new GSObject();
            params.put(GigyaProvider.GigyaConstants.REQ_KEY, regToken);
            GigyaProvider.sendRequest(GigyaProvider.GigyaConstants.GIGYA_FINALIZE_REGISTER, params, null, gigyaListener, context);
        } catch (Exception e) {
            gigyaListener.onError(e.getMessage());
        }
    }


    void handleFinalRegistrationResult(int errorCode, GigyaListener gigyaListener) {
        try {
            if (errorCode == 0) {
                gigyaListener.onSuccess(context.getResources().getString(R.string.registerSuccess));
            } else if (errorCode == 206002)// account pending verification
            {
                gigyaListener.onSuccess(context.getResources().getString(R.string.accountPendingVerification));
            }

        } catch (Exception ex) {
            gigyaListener.onError(ex.toString());

        }
    }


    void handleInitRegistrationResult(int errorCode, GSResponse response, GigyaListener gigyaListener, Context context) {
        try {
            if (errorCode == 0) {
                regToken = response.getString(GigyaProvider.GigyaConstants.REQ_KEY, "");
                if (regToken != null && regToken.length() > 0) {
                    GigyaProvider.getInstance().executeQueuedOperation(gigyaListener, regToken, context);
                } else {
                    gigyaListener.onError(context.getResources().getString(R.string.errorMsg));

                }
            }
        } catch (Exception ex) {
            gigyaListener.onError(ex.toString());

        }
    }

    void handleRegistrationResult(int errorCode, GSResponse response, GigyaListener gigyaListener) {
        try {
            if (errorCode == 206001) {
                String token = response.getString(GigyaProvider.GigyaConstants.REQ_KEY, "");
                finalizeRegistration(token, gigyaListener, context);
                return;
            } else if (errorCode == 400009 || errorCode == 400006)// invalid parameter
            {
                regToken = null;
                gigyaListener.onError(context.getResources().getString(R.string.emailUsedBefore));
            }
        } catch (Exception ex) {
            gigyaListener.onError(ex.toString());

        }
    }

}

